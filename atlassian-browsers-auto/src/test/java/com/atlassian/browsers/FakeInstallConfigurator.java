package com.atlassian.browsers;

import org.mockito.Mockito;

/**
 *
 * @since 2.5
 */
class FakeInstallConfigurator extends AbstractInstallConfigurator
{
    static InstallConfigurator spy()
    {
        return Mockito.spy(new FakeInstallConfigurator());
    }
}
