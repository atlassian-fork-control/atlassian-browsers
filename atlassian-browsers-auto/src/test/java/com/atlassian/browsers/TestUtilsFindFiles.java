package com.atlassian.browsers;

import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.Matchers.emptyIterable;
import static org.junit.Assert.*;

public class TestUtilsFindFiles
{
    @Rule
    public final TemporaryFolder temporaryFolder = new TemporaryFolder();

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void findFilesShouldNotAllowNullDirectory() throws IOException
    {
        Utils.findFiles(null, "one", "two", "three");
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void findFilesShouldNotAllowNullNames() throws IOException
    {
        Utils.findFiles(temporaryFolder.getRoot(), (String[]) null);
    }

    @Test
    public void findFilesShouldNotFindAnyFiles() throws Exception
    {
        Iterable<File> result = Utils.findFiles(temporaryFolder.getRoot(), "one", "two", "three");
        assertNotNull(result);
        assertThat(result, emptyIterable());
    }

    @Test
    public void shouldFindSomeFiles() throws Exception
    {
        temporaryFolder.newFile("one");
        temporaryFolder.newFile("two");
        File subdir = temporaryFolder.newFolder("subdir");
        assertTrue(new File(subdir, "one").createNewFile());

        Iterable<File> result = Utils.findFiles(temporaryFolder.getRoot(), "one", "two", "three");
        assertNotNull(result);
        assertThat(result, Matchers.<File>iterableWithSize(3));
    }
}
