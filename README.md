Atlassian Browsers
==================

## Summary ##

This is a small Java library that allows mavenized projects to automatically pick up appropriate browser artifacts
(Chrome, Firefox, PhantomJS) depending on the local operating system and architecture.

## Usage ##

### Upgrading to the latest browsers ###

1. Check the latest version of the browsers and update the corresponding properties in [`pom.xml`](pom.xml):

   |  Browser        |  POM Properties                              |  URL                                                      |
   | --------------- | -------------------------------------------- | --------------------------------------------------------- |
   | Chrome          | `chrome.version`, `chrome.profile.version`   | https://chromereleases.googleblog.com/                    |
   | Firefox         | `firefox.version`, `firefox.profile.version` | https://wiki.mozilla.org/Releases                         |
   | Gecko Driver    | `geckodriver.version`                        | https://github.com/mozilla/geckodriver/releases           |
   | IE Driver       | `ie.version`                                 | http://selenium-release.storage.googleapis.com/index.html |
   
   For example:
   
   ```
   <chrome.version>75.0.3770.90</chrome.version>
   <chrome.profile.version>75.0.3770.90</chrome.profile.version>
   <firefox.version>67.0.2</firefox.version>
   <firefox.profile.version>67.0.2</firefox.profile.version>
   <geckodriver.version>v0.24.0</geckodriver.version>
   <ie.profile.version>3.141.59</ie.profile.version>
   ```
   
2. Run `scripts/update-all.sh` and optionally deploy all artifacts with the `--deploy` argument.


Alternatively you can deploy each browser/os combination manually:

1. Run `scripts/update-browser.sh` for given version/OS combination, to get and package (and optionally deploy) the browser binaries. Run `scripts/update-browser.sh --help` for details.

      example: ``$ ./update-browser.sh chrome osx 36``

2. Run `scripts/update-<browser>-profile.sh` to get and package (and optionally deploy) the browser profile.  Run `scripts/update-<browser>-profile.sh --help` for details.

      example: ``$ ./update-chrome-profile.sh 2.12 osx``